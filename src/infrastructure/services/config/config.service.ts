import { Injectable } from '@nestjs/common';
import { ConfigService as NestConfigService } from '@nestjs/config';
import { ConfigService as IConfigService } from '&/domain/services';

@Injectable()
export class ConfigService implements IConfigService {
  constructor(private readonly nestConfig: NestConfigService) {}

  public get<T>(key: string): T | undefined {
    return this.nestConfig.get<T>(key);
  }
}
