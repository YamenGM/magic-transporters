import { Envs } from '&/application/constants';
import { Type, plainToInstance } from 'class-transformer';
import { IsNumber, validateSync } from 'class-validator';

class EnvironmentVariables {
  @IsNumber()
  @Type(() => Number)
  [Envs.PORT]!: number;
}

export function validate(config: Record<string, unknown>) {
  const validatedConfig = plainToInstance(EnvironmentVariables, config);

  const errors = validateSync(validatedConfig, {
    skipMissingProperties: false,
  });

  if (errors.length > 0) {
    throw new Error(errors.toString());
  }

  return validatedConfig;
}
