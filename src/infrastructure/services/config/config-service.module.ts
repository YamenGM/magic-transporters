import { ConfigService as IConfigService } from '&/domain/services';
import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { ConfigService } from './config.service';
import { validate } from './config.validation';

@Module({
  providers: [
    ConfigService,
    { provide: IConfigService, useExisting: ConfigService },
  ],
  imports: [ConfigModule.forRoot({ validate: validate })],
  exports: [IConfigService],
})
export class ConfigServiceModule {}
