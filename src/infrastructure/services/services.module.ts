import { Module } from '@nestjs/common';
import { ConfigServiceModule } from './config';

@Module({
  imports: [ConfigServiceModule],
  exports: [ConfigServiceModule],
})
export class ServicesModule {}
