import { ItemInMemoryModel } from '../../items/models/item-in-memory.model';
import { MoverInMemoryModel } from '../../movers/models/mover-in-memory.model';

export interface MissionInMemoryModel {
  id: number;
  mover_id: number;
  loading_at: string;
  on_mission_at: string | null;
  done_at: string | null;
  created_at: string;
  updated_at: string;
  deleted_at: string | null;
  // Relationq
  mover?: MoverInMemoryModel;
  items?: ItemInMemoryModel[];
}
