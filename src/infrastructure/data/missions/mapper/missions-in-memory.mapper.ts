import { Inject, Injectable } from '@nestjs/common';
import { ItemsMapper, MissionsMapper, MoversMapper } from '&/domain/mapper';
import { Mission } from '&/domain/entites';
import { MissionInMemoryModel } from '../models/mission-in-memory.model';
import { MoverInMemoryModel } from '../../movers/models/mover-in-memory.model';
import { ItemInMemoryModel } from '../../items/models/item-in-memory.model';

@Injectable()
export class MissionsInMemoryMapper
  implements MissionsMapper<MissionInMemoryModel>
{
  constructor(
    @Inject(MoversMapper)
    private readonly moversMapper: MoversMapper<MoverInMemoryModel>,
    @Inject(ItemsMapper)
    private readonly itemsMapper: ItemsMapper<ItemInMemoryModel>,
  ) {}
  public toModel(entity: Mission): MissionInMemoryModel {
    return {
      id: entity.id!,
      mover_id: entity.moverId,
      loading_at: entity.loadingAt ? String(entity.loadingAt) : null,
      on_mission_at: entity.onMissionAt ? String(entity.onMissionAt) : null,
      done_at: entity.doneAt ? String(entity.doneAt) : null,
      created_at: String(entity.createdAt), //HI
      updated_at: String(entity.updatedAt),
      deleted_at: entity.isDeleted() ? String(entity.deletedAt) : null,
    };
  }

  public toEntity(model: MissionInMemoryModel): Mission {
    const entity = new Mission({
      id: model.id,
      moverId: model.mover_id,
      loadingAt: model.loading_at ? new Date(model.loading_at) : null,
      onMissionAt: model.on_mission_at ? new Date(model.on_mission_at) : null,
      doneAt: model.done_at ? new Date(model.done_at) : null,
      createdAt: new Date(model.created_at),
      updatedAt: new Date(model.updated_at),
      deletedAt: model.deleted_at ? new Date(model.deleted_at) : null,
    });

    if (model.mover) entity.mover = this.moversMapper.toEntity(model.mover);
    if (model.items && model.items.length !== 0)
      entity.items = model.items.map((item) => this.itemsMapper.toEntity(item));
    return entity;
  }
}
