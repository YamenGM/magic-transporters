import { Module, forwardRef } from '@nestjs/common';
import { MissionsRepository } from '&/domain/repositories';
import { MissionsInMemoryMapper } from './mapper';
import { MissionInMemoryRepository } from './repository';
import { MissionsMapper, MoversMapper } from '&/domain/mapper';
import { DataModule } from '../data.module';

@Module({
  imports: [forwardRef(() => DataModule)],
  providers: [
    MissionsInMemoryMapper,
    MissionInMemoryRepository,
    { provide: MissionsMapper, useExisting: MissionsInMemoryMapper },
    { provide: MissionsRepository, useExisting: MissionInMemoryRepository },
  ],
  exports: [MissionsRepository, MissionsMapper],
})
export class MissionsDataModule {}
