import { EntityNotFoundException } from '&/application/exceptions';
import {
  ItemsRepository,
  MissionsRepository,
  MoversRepository,
} from '&/domain/repositories';
import { Inject, Injectable } from '@nestjs/common';
import { ComplatedMissionType } from '&/domain/types';
import { Item, Mission } from '&/domain/entites';
import MissionsMock from '../mock/missions.json';
import { MissionInMemoryModel } from '../models/mission-in-memory.model';
import { MissionsInMemoryMapper } from '../mapper';
import { ItemInMemoryModel } from '../../items/models/item-in-memory.model';
import { ItemsMapper, MoversMapper } from '&/domain/mapper';
import { MoverInMemoryModel } from '../../movers/models/mover-in-memory.model';

@Injectable()
export class MissionInMemoryRepository implements MissionsRepository {
  private _missionsMock: MissionInMemoryModel[] = MissionsMock;

  constructor(
    // private readonly prismaService: PrismaService,
    private readonly mapper: MissionsInMemoryMapper,
    @Inject(MoversRepository)
    private readonly moversRepository: MoversRepository,
    @Inject(ItemsRepository)
    private readonly itemsRepository: ItemsRepository,
    @Inject(MoversMapper)
    private readonly moversMapper: MoversMapper<MoverInMemoryModel>,
    @Inject(ItemsMapper)
    private readonly itemsMapper: ItemsMapper<ItemInMemoryModel>,
  ) {}

  public async getMostCompletedMissions(): Promise<ComplatedMissionType[]> {
    const movers = await this.moversRepository.findMany();
    const complatedMissions: ComplatedMissionType[] = movers.map((mover) => {
      const missionsForMover = this._missionsMock.filter(
        (missionMock) =>
          missionMock.mover_id === mover.id && missionMock.done_at,
      );
      return {
        moverId: mover.id,
        missionsId: missionsForMover.map((m) => m.id),
        count: missionsForMover.length,
      };
    });

    return complatedMissions.sort((mis1, mis2) => mis2.count - mis1.count);
  }

  public async findOne({
    id,
  }: MissionsRepository.FindOneFilter): Promise<Mission | null> {
    const Mission = this._missionsMock.find((Mission) => Mission.id === id);

    if (!Mission) {
      return null;
    }

    return this.mapper.toEntity(Mission);
  }

  public async findMany(
    options?: MissionsRepository.FindAllOptions,
  ): Promise<Mission[]> {
    const { limit = 10, skip = 0 } = options || {};

    const missions: MissionInMemoryModel[] = [];
    for (let i = 0; i < this._missionsMock.length; i++) {
      const mover = await this.moversRepository.findOne({
        id: this._missionsMock[i].mover_id,
      });

      let items: Item[] = [];
      const allItems = await this.itemsRepository.findMany();
      items = allItems.filter(
        (item) => item.missionId === this._missionsMock[i].id,
      );

      missions.push({
        ...this._missionsMock[i],
        items:
          items.length !== 0
            ? items.map((item) => this.itemsMapper.toModel(item))
            : null,
        mover: this.moversMapper.toModel(mover),
      });
    }

    return missions.map((mission) => this.mapper.toEntity(mission));
  }

  public async create(mission: Mission): Promise<Mission> {
    const results = this._missionsMock.push({
      ...this.mapper.toModel(mission),
    });
    return mission;
  }

  public async updateOne(mission: Mission): Promise<Mission> {
    await this.checkMissionExists({ id: mission.id });

    const i = this._missionsMock.findIndex((index) => index.id === mission.id);
    mission.setUpdateDate();

    this._missionsMock[i] = this.mapper.toModel(mission);

    return this.mapper.toEntity(this._missionsMock[i]);
  }

  private async checkMissionExists({
    id,
  }: MoversRepository.FindOneFilter): Promise<Boolean> {
    const mission = this._missionsMock.find((mission) => mission.id === id);

    if (!mission)
      throw new EntityNotFoundException({
        id,
        message: `Mission with id ${id} not found`,
      });

    return true;
  }
}
