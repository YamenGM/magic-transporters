import { EntityNotFoundException } from '&/application/exceptions';
import { ItemsRepository } from '&/domain/repositories';
import { Injectable } from '@nestjs/common';
import { ItemsInMemoryMapper } from '../mapper/items-in-memory.mapper';
import { Item } from '&/domain/entites';
import ItemsMock from '../mock/items.json';
import { ItemInMemoryModel } from '../models/item-in-memory.model';

@Injectable()
export class ItemInMemoryRepository implements ItemsRepository {
  private _itemsMock: ItemInMemoryModel[] = ItemsMock;
  constructor(
    // private readonly prismaService: PrismaService,
    private readonly mapper: ItemsInMemoryMapper,
  ) {}

  public async findOne({
    id,
  }: ItemsRepository.FindOneFilter): Promise<Item | null> {
    const item = this._itemsMock.find((mover) => mover.id === id);

    if (!item) {
      return null;
    }

    return this.mapper.toEntity(item);
  }

  public async findMany(
    options?: ItemsRepository.FindAllOptions,
  ): Promise<Item[]> {
    // const { limit = 10, skip = 0 } = options || {};

    return this._itemsMock.map((item) => this.mapper.toEntity(item));
  }

  public async create(item: Item): Promise<Item> {
    const results = this._itemsMock.push({
      ...this.mapper.toModel(item),
    });
    return item;
  }

  public async updateOne(item: Item): Promise<Item> {
    await this.checkItemExists({ id: item.id });

    const i = this._itemsMock.findIndex((index) => index.id === item.id);
    item.setUpdateDate();

    this._itemsMock[i] = this.mapper.toModel(item);

    return this.mapper.toEntity(this._itemsMock[i]);
  }

  private async checkItemExists({
    id,
  }: ItemsRepository.FindOneFilter): Promise<Boolean> {
    const item = this._itemsMock.find((item) => item.id === id);

    if (!item)
      throw new EntityNotFoundException({
        id,
        message: `Item with id ${id} not found`,
      });

    return true;
  }
}
