export interface ItemInMemoryModel {
  id: number;
  name: string;
  weight: number;
  mission_id: number | null;
  created_at: string;
  updated_at: string;
  deleted_at: string | null;
}
