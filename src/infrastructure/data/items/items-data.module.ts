import { Module } from '@nestjs/common';
import { ItemsRepository } from '&/domain/repositories';
import { ItemsInMemoryMapper } from './mapper';
import { ItemInMemoryRepository } from './repository';
import { ItemsMapper } from '&/domain/mapper';

@Module({
  imports: [],
  providers: [
    ItemsInMemoryMapper,
    ItemInMemoryRepository,
    { provide: ItemsMapper, useExisting: ItemsInMemoryMapper },
    { provide: ItemsRepository, useExisting: ItemInMemoryRepository },
  ],
  exports: [ItemsRepository, ItemsMapper],
})
export class ItemsDataModule {}
