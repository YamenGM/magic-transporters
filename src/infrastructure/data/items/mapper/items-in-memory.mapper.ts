import { Injectable } from '@nestjs/common';
import { ItemsMapper } from '&/domain/mapper';
import { Item } from '&/domain/entites';
import { ItemInMemoryModel } from '../models/item-in-memory.model';

@Injectable()
export class ItemsInMemoryMapper implements ItemsMapper<ItemInMemoryModel> {
  public toModel(entity: Item): ItemInMemoryModel {
    return {
      id: entity.id!,
      name: entity.name,
      weight: entity.weight,
      mission_id: entity.missionId,
      created_at: String(entity.createdAt), //HI
      updated_at: String(entity.updatedAt),
      deleted_at: entity.isDeleted() ? String(entity.deletedAt) : null,
    };
  }

  public toEntity(model: ItemInMemoryModel): Item {
    const item = new Item({
      id: model.id,
      weight: model.weight,
      name: model.name,
      missionId: model.mission_id,
      createdAt: new Date(model.created_at),
      updatedAt: new Date(model.updated_at),
      deletedAt: model.deleted_at ? new Date(model.deleted_at) : null,
    });

    return item;
  }
}
