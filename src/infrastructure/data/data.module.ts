import { Module } from '@nestjs/common';
import { MoversDataModule } from './movers/movers-data.module';
import { ItemsDataModule } from './items/items-data.module';
import { MissionsDataModule } from './missions/missions-data.module';

const dataModules = [MoversDataModule, ItemsDataModule, MissionsDataModule];

@Module({
  imports: [...dataModules],
  exports: dataModules,
})
export class DataModule {}
