export enum mover_status {
  resting = 'resting',
  loading = 'loading',
  mission = 'mission',
  done = 'done',
}

export interface MoverInMemoryModel {
  id: number;
  weight_limit: number;
  energy: number;
  quest_state: string;
  created_at: string;
  updated_at: string;
  deleted_at: string | null;
}
