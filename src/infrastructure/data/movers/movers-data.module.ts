import { Module } from '@nestjs/common';
import { MoversRepository } from '&/domain/repositories';
import { MoversInMemoryMapper } from './mapper';
import { MoverInMemoryRepository } from './repository';
import { MoversMapper } from '&/domain/mapper';

@Module({
  imports: [],
  providers: [
    MoversInMemoryMapper,
    MoverInMemoryRepository,
    { provide: MoversMapper, useExisting: MoversInMemoryMapper },
    { provide: MoversRepository, useExisting: MoverInMemoryRepository },
  ],
  exports: [MoversRepository, MoversMapper],
})
export class MoversDataModule {}
