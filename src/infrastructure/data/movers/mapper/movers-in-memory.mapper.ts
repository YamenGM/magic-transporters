import { Injectable } from '@nestjs/common';
import { MoverStatus } from '&/domain/enums';
import { MoversMapper } from '&/domain/mapper';
import { Mover } from '&/domain/entites';
import {
  MoverInMemoryModel,
  mover_status,
} from '../models/mover-in-memory.model';

@Injectable()
export class MoversInMemoryMapper implements MoversMapper<MoverInMemoryModel> {
  public toModel(entity: Mover): MoverInMemoryModel {
    const status = {
      [MoverStatus.RESTING]: mover_status.resting,
      [MoverStatus.LOADING]: mover_status.loading,
      [MoverStatus.MISSION]: mover_status.mission,
      [MoverStatus.DONE]: mover_status.done,
    }[entity.questState];

    return {
      id: entity.id!,
      weight_limit: entity.weightLimit,
      energy: entity.energy,
      quest_state: status,
      created_at: String(entity.createdAt), //HI
      updated_at: String(entity.updatedAt),
      deleted_at: entity.isDeleted() ? String(entity.deletedAt) : null,
    };
  }

  public toEntity(model: MoverInMemoryModel): Mover {
    const status = {
      [mover_status.resting]: MoverStatus.RESTING,
      [mover_status.loading]: MoverStatus.LOADING,
      [mover_status.mission]: MoverStatus.MISSION,
      [mover_status.done]: MoverStatus.DONE,
    }[model.quest_state];

    const mover = new Mover({
      id: model.id,
      weightLimit: model.weight_limit,
      energy: model.energy,
      questState: status,
      createdAt: new Date(model.created_at),
      updatedAt: new Date(model.updated_at),
      deletedAt: model.deleted_at ? new Date(model.deleted_at) : null,
    });

    return mover;
  }
}
