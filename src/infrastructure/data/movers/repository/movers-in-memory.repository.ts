import { EntityNotFoundException } from '&/application/exceptions';
import { MoversRepository } from '&/domain/repositories';
import { Injectable } from '@nestjs/common';
import { MoversInMemoryMapper } from '../mapper/movers-in-memory.mapper';
import { Mover } from '&/domain/entites';
import MoversMock from '../mock/movers.json';
import { MoverInMemoryModel } from '../models/mover-in-memory.model';

@Injectable()
export class MoverInMemoryRepository implements MoversRepository {
  private _moversMock: MoverInMemoryModel[] = MoversMock;

  constructor(private readonly mapper: MoversInMemoryMapper) {}

  public async findOne({
    id,
  }: MoversRepository.FindOneFilter): Promise<Mover | null> {
    const mover = this._moversMock.find((mover) => mover.id === id);

    if (!mover) {
      return null;
    }

    return this.mapper.toEntity(mover);
  }

  public async findMany(
    options?: MoversRepository.FindAllOptions,
  ): Promise<Mover[]> {
    // const { limit = 10, skip = 0 } = options || {};

    return this._moversMock.map((item) => this.mapper.toEntity(item));
  }

  public async create(mover: Mover): Promise<Mover> {
    const results = this._moversMock.push({
      ...this.mapper.toModel(mover),
    });
    return mover;
  }

  public async updateOne(mover: Mover): Promise<Mover> {
    await this.checkMoverExists({ id: mover.id });

    const i = this._moversMock.findIndex((index) => index.id === mover.id);
    // mover.setUpdateDate();

    this._moversMock[i] = this.mapper.toModel(mover);

    return this.mapper.toEntity(this._moversMock[i]);
  }

  private async checkMoverExists({
    id,
  }: MoversRepository.FindOneFilter): Promise<Boolean> {
    const mover = this._moversMock.find((mover) => mover.id === id);

    if (!mover)
      throw new EntityNotFoundException({
        id,
        message: `Mover with id ${id} not found`,
      });

    return true;
  }
}
