import { UsePresenter } from '&/presentation/common/decorators';
import { Body, Controller, Get, Param, Post, Put, Query } from '@nestjs/common';
import { ListMissionsPresenter, MutateMissionPresenter } from './presenter';
import {
  AddMission,
  ListMissions,
  UpdateMission,
} from '&/application/usecases/missions';
import { AddMissionDto, ListMissionsDto, StartOrEndMissionDto } from './dto';
import { GetMostCompletedMissions } from '&/application/usecases/missions/most-complated-missions.usecase';

@Controller('missions')
export class MissionsController {
  constructor(
    private readonly listMissionsUC: ListMissions.UseCase,
    private readonly addMissionUC: AddMission.UseCase,
    private readonly updateMissionUC: UpdateMission.UseCase,
    private readonly getMostCompletedMissionsUC: GetMostCompletedMissions.UseCase,
  ) {}

  @Get('/')
  @UsePresenter(ListMissionsPresenter)
  public async listMissions(@Query() listMissionsDto: ListMissionsDto) {
    return await this.listMissionsUC.execute({
      ...listMissionsDto,
    });
  }

  // @UsePresenter(ListMissionsPresenter)
  @Get('/most')
  public async getMostCompletedMissions() {
    return await this.getMostCompletedMissionsUC.execute({});
  }

  @Post()
  @UsePresenter(MutateMissionPresenter)
  public async addMission(@Body() addMissionDto: AddMissionDto) {
    return this.addMissionUC.execute({
      ...addMissionDto,
    });
  }

  @Put('/:id')
  @UsePresenter(MutateMissionPresenter)
  public async updateMission(
    @Param('id') id: number,
    @Body() startOrEndMissionDto: StartOrEndMissionDto,
  ) {
    return await this.updateMissionUC.execute({
      id: id,
      data: { MissionStatus: startOrEndMissionDto.MissionStatus },
    });
  }
}
