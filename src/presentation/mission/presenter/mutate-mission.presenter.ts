import { BasePresenter } from '&/presentation/common/interfaces';
import {
  BaseException,
  EntityNotFoundException,
} from '&/application/exceptions';
import {
  BadRequestException,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { MissionVm } from '../vm';
import { Mission } from '&/domain/entites';

export class MutateMissionPresenter extends BasePresenter<Mission, MissionVm> {
  presentData(data: Mission): MissionVm {
    return new MissionVm(data);
  }

  presentError(exception: BaseException) {
    console.error(exception);
    if (exception instanceof EntityNotFoundException) {
      throw new NotFoundException(exception.message);
    } else if (exception instanceof BadRequestException) {
      // dto validation exception
      throw exception;
    } else {
      throw new InternalServerErrorException(exception.message);
    }
  }
}
