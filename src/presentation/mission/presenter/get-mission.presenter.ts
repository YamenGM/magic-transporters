import { BasePresenter } from '&/presentation/common/interfaces';
import { Mission } from '&/domain/entites';
import {
  BaseException,
  EntityNotFoundException,
} from '&/application/exceptions';
import {
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { MissionVm } from '../vm';

export class GetMissionPresenter extends BasePresenter<Mission, MissionVm> {
  presentData(data: Mission): MissionVm {
    return new MissionVm(data);
  }

  presentError(exception: BaseException) {
    if (exception instanceof EntityNotFoundException) {
      throw new NotFoundException();
    } else {
      throw new InternalServerErrorException();
    }
  }
}
