import { BasePresenter } from '&/presentation/common/interfaces';
import { Mission } from '&/domain/entites';
import { BaseException } from '&/application/exceptions';
import { Page } from '&/domain/types';
import { MissionsListVm } from '../vm';
import { InternalServerErrorException } from '@nestjs/common';

export class ListMissionsPresenter extends BasePresenter<
  Page<Mission>,
  MissionsListVm
> {
  presentData(data: Page<Mission>): MissionsListVm {
    return new MissionsListVm(data);
  }

  presentError(exception: BaseException) {
    console.error(exception);
    throw new InternalServerErrorException(exception.message);
  }
}
