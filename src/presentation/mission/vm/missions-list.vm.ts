import { Mission } from '&/domain/entites';
import { Page } from '&/domain/types';
import { MissionVm } from './mission.vm';

export class MissionsListVm {
  data: MissionVm[];

  page: number;

  perPage: number;

  constructor(page: Page<Mission>) {
    this.data = page.data.map((mission) => new MissionVm(mission));
    this.page = page.page;
    this.perPage = page.perPage;
  }
}
