import { Item, Mission, Mover } from '&/domain/entites';
import { MoverStatus } from '&/domain/enums';
import { IdOf } from '&/domain/types';

export class MissionVm {
  id: number;
  loadingAt: Date;
  onMissionAt: Date | null;
  doneAt: Date;

  mover: {
    id: IdOf<Mover>;
    weightLimit: number;
    energy: number;
    questState: MoverStatus;
  };

  items: {
    id: IdOf<Item>;
    name: string;
    weight: number;
  }[];

  constructor(mission: Mission) {
    this.id = mission.id!;
    this.loadingAt = mission.loadingAt;
    this.onMissionAt = mission.onMissionAt;
    this.doneAt = mission.doneAt;
    this.mover = {
      id: mission.mover.id,
      weightLimit: mission.mover.weightLimit,
      energy: mission.mover.energy,
      questState: mission.mover.questState,
    };

    this.items = mission.items?.map((item) => ({
      id: item.id,
      name: item.name,
      weight: item.weight,
    }));
  }
}
