import { Type } from 'class-transformer';
import { ArrayNotEmpty, IsArray, IsInt } from 'class-validator';

export class AddMissionDto {
  @IsInt()
  @Type(() => Number)
  public moverId: number;

  @IsArray()
  @ArrayNotEmpty()
  @IsInt({ each: true })
  public itemsId: number[];
}
