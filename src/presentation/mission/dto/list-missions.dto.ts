import { PaginatedDto } from '&/presentation/common/dto';

export class ListMissionsDto extends PaginatedDto {}
