import { IsEnum } from 'class-validator';

enum MissionStatus {
  START = 'start',
  END = 'end',
}

export class StartOrEndMissionDto {
  @IsEnum(MissionStatus)
  MissionStatus: MissionStatus;
}
