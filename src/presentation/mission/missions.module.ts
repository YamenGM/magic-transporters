import { Module } from '@nestjs/common';
import { MissionsController } from './missions.controller';
import { MissionsUcDi } from '&/di/usecases/missions-uc.di';

@Module({
  controllers: [MissionsController],
  imports: [MissionsUcDi],
})
export class MissionsModule {}
