import { BasePresenter } from '&/presentation/common/interfaces';
import {
  BaseException,
  EntityNotFoundException,
} from '&/application/exceptions';
import {
  BadRequestException,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { ItemVm } from '../vm';
import { Item } from '&/domain/entites';

export class MutateItemPresenter extends BasePresenter<Item, ItemVm> {
  presentData(data: Item): ItemVm {
    return new ItemVm(data);
  }

  presentError(exception: BaseException) {
    console.error(exception);
    if (exception instanceof EntityNotFoundException) {
      throw new NotFoundException(exception.message);
    } else if (exception instanceof BadRequestException) {
      // dto validation exception
      throw exception;
    } else {
      throw new InternalServerErrorException(exception.message);
    }
  }
}
