import { BasePresenter } from '&/presentation/common/interfaces';
import { Item } from '&/domain/entites';
import { BaseException } from '&/application/exceptions';
import { Page } from '&/domain/types';
import { ItemsListVm } from '../vm';
import { InternalServerErrorException } from '@nestjs/common';

export class ListItemsPresenter extends BasePresenter<Page<Item>, ItemsListVm> {
  presentData(data: Page<Item>): ItemsListVm {
    return new ItemsListVm(data);
  }

  presentError(exception: BaseException) {
    console.error(exception);
    throw new InternalServerErrorException(exception.message);
  }
}
