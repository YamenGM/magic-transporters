import { BasePresenter } from '&/presentation/common/interfaces';
import { Item } from '&/domain/entites';
import {
  BaseException,
  EntityNotFoundException,
} from '&/application/exceptions';
import {
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { ItemVm } from '../vm';

export class GetItemPresenter extends BasePresenter<Item, ItemVm> {
  presentData(data: Item): ItemVm {
    return new ItemVm(data);
  }

  presentError(exception: BaseException) {
    if (exception instanceof EntityNotFoundException) {
      throw new NotFoundException();
    } else {
      throw new InternalServerErrorException();
    }
  }
}
