import { ItemsUcDi } from '&/di/usecases';
import { Module } from '@nestjs/common';
import { ItemsController } from './items.controller';

@Module({
  controllers: [ItemsController],
  imports: [ItemsUcDi],
})
export class ItemsModule {}
