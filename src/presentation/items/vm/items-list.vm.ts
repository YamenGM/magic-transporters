import { Item } from '&/domain/entites';
import { Page } from '&/domain/types';
import { ItemVm } from './item.vm';

export class ItemsListVm {
  data: ItemVm[];

  page: number;

  perPage: number;

  constructor(page: Page<Item>) {
    this.data = page.data.map((item) => new ItemVm(item));
    this.page = page.page;
    this.perPage = page.perPage;
  }
}
