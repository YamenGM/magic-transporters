import { Item } from '&/domain/entites';

export class ItemVm {
  id: number;
  name: string;
  weight: number;

  constructor(item: Item) {
    this.id = item.id!;
    this.name = item.name;
    this.weight = item.weight;
  }
}
