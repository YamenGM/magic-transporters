import { Type } from 'class-transformer';
import { IsInt, IsNotEmpty, IsString } from 'class-validator';

export class AddItemDto {
  @IsString()
  @IsNotEmpty()
  public name: string;

  @IsInt()
  @Type(() => Number)
  public weight: number;
}
