import { Type } from 'class-transformer';
import { IsInt, IsNotEmpty, IsOptional, IsString } from 'class-validator';

export class UpdateMoverDto {
  @IsString()
  @IsNotEmpty()
  @IsOptional()
  public name?: string;

  @IsInt()
  @IsOptional()
  @Type(() => Number)
  public weight?: number;
}
