import { PaginatedDto } from '&/presentation/common/dto';

export class ListItemsDto extends PaginatedDto {}
