import { UsePresenter } from '&/presentation/common/decorators';
import { Body, Controller, Get, Post, Query } from '@nestjs/common';
import { ListItemsDto, AddItemDto } from './dto';
import { ListItemsPresenter, MutateItemPresenter } from './presenter';
import { AddItem, ListItems } from '&/application/usecases/items';

@Controller('items')
export class ItemsController {
  constructor(
    private readonly listItemsUC: ListItems.UseCase,
    private readonly addItemUC: AddItem.UseCase,
  ) {}

  @Get('/')
  @UsePresenter(ListItemsPresenter)
  public async listItems(@Query() listItemsDto: ListItemsDto) {
    return await this.listItemsUC.execute({
      ...listItemsDto,
    });
  }

  @Post()
  @UsePresenter(MutateItemPresenter)
  public async addItem(@Body() addItemDto: AddItemDto) {
    return this.addItemUC.execute({
      ...addItemDto,
    });
  }
}
