import { Mover } from '&/domain/entites';
import { MoverStatus } from '&/domain/enums';

export class MoverVm {
  id: number;
  weightLimit: number;
  energy: number;
  questState: string;

  constructor(mover: Mover) {
    const status = {
      [MoverStatus.RESTING]: 'resting',
      [MoverStatus.LOADING]: 'loading',
      [MoverStatus.MISSION]: 'mission',
      [MoverStatus.DONE]: 'done',
    }[mover.questState];

    this.id = mover.id!;
    this.weightLimit = mover.weightLimit;
    this.energy = mover.energy;
    this.questState = status;
  }
}
