import { Mover } from '&/domain/entites';
import { Page } from '&/domain/types';
import { MoverVm } from './mover.vm';

export class MoversListVm {
  data: MoverVm[];

  page: number;

  perPage: number;

  constructor(page: Page<Mover>) {
    this.data = page.data.map((mover) => new MoverVm(mover));
    this.page = page.page;
    this.perPage = page.perPage;
  }
}
