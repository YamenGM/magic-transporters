import { ListMovers, AddMover } from '&/application/usecases/movers';
import { UsePresenter } from '&/presentation/common/decorators';
import { Body, Controller, Get, Post, Query } from '@nestjs/common';
import { ListMoversDto } from './dto';
import { ListMoversPresenter, MutateMoverPresenter } from './presenter';

import { AddMoverDto } from './dto/add-mover.dto';

@Controller('movers')
export class MoversController {
  constructor(
    private readonly listMoversUC: ListMovers.UseCase,
    private readonly addMoverUC: AddMover.UseCase,
  ) {}

  @Get('/')
  @UsePresenter(ListMoversPresenter)
  public async listMovers(@Query() listMoversDto: ListMoversDto) {
    return await this.listMoversUC.execute({
      ...listMoversDto,
    });
  }

  @Post()
  @UsePresenter(MutateMoverPresenter)
  public async addMover(@Body() addMoverDto: AddMoverDto) {
    return this.addMoverUC.execute({
      ...addMoverDto,
    });
  }
}
