import { BasePresenter } from '&/presentation/common/interfaces';
import { Mover } from '&/domain/entites';
import {
  BaseException,
  EntityNotFoundException,
} from '&/application/exceptions';
import {
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { MoverVm } from '../vm';

export class GetMoverPresenter extends BasePresenter<Mover, MoverVm> {
  presentData(data: Mover): MoverVm {
    return new MoverVm(data);
  }

  presentError(exception: BaseException) {
    if (exception instanceof EntityNotFoundException) {
      throw new NotFoundException();
    } else {
      throw new InternalServerErrorException();
    }
  }
}
