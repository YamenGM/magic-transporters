import { BasePresenter } from '&/presentation/common/interfaces';
import {
  BaseException,
  EntityNotFoundException,
} from '&/application/exceptions';
import {
  BadRequestException,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { MoverVm } from '../vm';
import { Mover } from '&/domain/entites';

export class MutateMoverPresenter extends BasePresenter<Mover, MoverVm> {
  presentData(data: Mover): MoverVm {
    return new MoverVm(data);
  }

  presentError(exception: BaseException) {
    console.error(exception);
    if (exception instanceof EntityNotFoundException) {
      throw new NotFoundException(exception.message);
    } else if (exception instanceof BadRequestException) {
      // dto validation exception
      throw exception;
    } else {
      throw new InternalServerErrorException(exception.message);
    }
  }
}
