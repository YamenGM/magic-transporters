import { BasePresenter } from '&/presentation/common/interfaces';
import { Mover } from '&/domain/entites';
import { BaseException } from '&/application/exceptions';
import { Page } from '&/domain/types';
import { MoversListVm } from '../vm';
import { InternalServerErrorException } from '@nestjs/common';

export class ListMoversPresenter extends BasePresenter<
  Page<Mover>,
  MoversListVm
> {
  presentData(data: Page<Mover>): MoversListVm {
    return new MoversListVm(data);
  }

  presentError(exception: BaseException) {
    console.error(exception);
    throw new InternalServerErrorException(exception.message);
  }
}
