import { Type } from 'class-transformer';
import { IsInt } from 'class-validator';

export class AddMoverDto {
  @IsInt()
  @Type(() => Number)
  public weightLimit: number;

  @IsInt()
  @Type(() => Number)
  public energy: number;
}
