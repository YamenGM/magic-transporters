import { MoverStatus } from '&/domain/enums';
import { Type } from 'class-transformer';
import { IsEnum, IsInt, IsOptional } from 'class-validator';

export class UpdateMoverDto {
  @IsInt()
  @IsOptional()
  @Type(() => Number)
  public weightLimit?: Number;

  @IsInt()
  @IsOptional()
  @Type(() => Number)
  public energy?: Number;

  @IsOptional()
  @IsEnum(MoverStatus)
  public questState?: MoverStatus;
}
