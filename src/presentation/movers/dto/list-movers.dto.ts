import { PaginatedDto } from '&/presentation/common/dto';

export class ListMoversDto extends PaginatedDto {}
