import { MoversUcDi } from '&/di/usecases';
import { Module } from '@nestjs/common';
import { MoversController } from './movers.controller';

@Module({
  controllers: [MoversController],
  imports: [MoversUcDi],
})
export class MoversModule {}
