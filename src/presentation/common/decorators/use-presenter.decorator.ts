import { SetMetadata, UseInterceptors, applyDecorators } from '@nestjs/common';
import { PresenterInterceptor } from '../interceptors';
import { BasePresenter } from '../interfaces';

/**
 * @param presenter presenter class to map the data from the controller
 * @returns view model mapped from the controller + adding swagger api
 */
export function UsePresenter<VM>(
  presenter: new (...args: any[]) => BasePresenter<unknown, VM>,
) {
  return applyDecorators(
    SetMetadata('presenter', presenter),
    UseInterceptors(PresenterInterceptor),
    // ApiResponse({ type: presenter }),
  );
}
