import { BaseException } from '&/application/exceptions';

export abstract class BasePresenter<T, VM = any> {
  abstract presentData(data: T): VM;
  abstract presentError(exception: BaseException): any;
}
