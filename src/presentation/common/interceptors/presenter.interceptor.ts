import {
  CallHandler,
  ExecutionContext,
  Injectable,
  NestInterceptor,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Observable, catchError, map, of } from 'rxjs';
import { BasePresenter } from '../interfaces';

@Injectable()
export class PresenterInterceptor<
  TResponse,
  TPresenter extends new (
    ...args: any[]
  ) => BasePresenter<
    TResponse,
    ReturnType<InstanceType<TPresenter>['presentData']>
  >,
> implements
    NestInterceptor<
      TResponse,
      ReturnType<InstanceType<TPresenter>['presentData']>
    >
{
  constructor(private readonly reflector: Reflector) {}

  /**
   * Intercepts the response and wraps it in the presenter
   * @param context
   * @param next
   */
  intercept(
    context: ExecutionContext,
    next: CallHandler,
  ): Observable<ReturnType<InstanceType<TPresenter>['presentData']>> {
    const Presenter = this.reflector.get<TPresenter>(
      'presenter',
      context.getHandler(),
    );
    const presenter = new Presenter();
    return next.handle().pipe(
      map((data) => presenter.presentData(data)),
      catchError((error) => of(presenter.presentError(error))),
    );
  }
}
