import { INestApplication } from '@nestjs/common';

export abstract class BaseBootstrap {
  protected app: INestApplication;

  constructor(app: INestApplication) {
    this.app = app;
  }

  public abstract run(): Promise<void>;
}
