import { INestApplication } from '@nestjs/common';
import { BaseBootstrap } from './base.bootstrap';
import { CorsBootstrap } from './cors.bootstrap';
import { ValidationBootstrap } from './validation.bootstrap';
import { ListenBootstrap } from './listen.bootstrap';

interface BootstrapConstructor {
  new (app: INestApplication): BaseBootstrap;
}

export default [
  CorsBootstrap,
  ValidationBootstrap,
  ListenBootstrap,
] as BootstrapConstructor[];
