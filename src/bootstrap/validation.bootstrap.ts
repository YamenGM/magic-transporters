import { ValidationPipe } from '@nestjs/common';
import { BaseBootstrap } from './base.bootstrap';

export class ValidationBootstrap extends BaseBootstrap {
  public async run(): Promise<void> {
    this.app.useGlobalPipes(
      new ValidationPipe({
        transform: true, // automatically transform payloads to be objects typed according to their DTO classes.
        whitelist: true, // skip all any properties that do not use any validation decorators.
      }),
    );
  }
}
