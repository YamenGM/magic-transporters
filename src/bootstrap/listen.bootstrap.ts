import { Envs } from '&/application/constants';
import { ConfigService } from '&/domain/services';
import { INestApplication } from '@nestjs/common';
import { BaseBootstrap } from './base.bootstrap';

export class ListenBootstrap extends BaseBootstrap {
  private readonly configService: ConfigService;

  constructor(app: INestApplication) {
    super(app);
    this.configService = app.get<ConfigService>(ConfigService);
  }

  public async run(): Promise<void> {
    const configService: ConfigService = this.app.get(ConfigService);
    const PORT = configService.get<number>(Envs.PORT)!;
    await this.app.listen(PORT);
  }
}
