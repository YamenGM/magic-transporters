import { BaseBootstrap } from './base.bootstrap';

export class CorsBootstrap extends BaseBootstrap {
  public async run(): Promise<void> {
    this.app.enableCors({ origin: '*' });
  }
}
