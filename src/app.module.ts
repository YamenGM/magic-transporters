import { Module } from '@nestjs/common';

import { DataModule } from './infrastructure/data/data.module';
import { MoversModule } from './presentation/movers/movers.module';
import { ItemsModule } from './presentation/items/items.module';
import { ServicesModule } from './infrastructure/services/services.module';
import { MissionsModule } from './presentation/mission/missions.module';

@Module({
  imports: [
    //
    DataModule,
    ServicesModule,

    //
    MoversModule,
    ItemsModule,
    MissionsModule,
  ],
})
export class AppModule {}
