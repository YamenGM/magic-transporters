import { AddItem, ListItems } from '&/application/usecases/items';
import { ItemsRepository } from '&/domain/repositories';
import { DataModule } from '&/infrastructure/data/data.module';
import { Module } from '@nestjs/common';

@Module({
  providers: [
    {
      provide: ListItems.UseCase,
      useFactory: (itemsRepository: ItemsRepository) => {
        return new ListItems.UseCase(itemsRepository);
      },
      inject: [ItemsRepository],
    },
    {
      provide: AddItem.UseCase,
      useFactory: (itemsRepository: ItemsRepository) => {
        return new AddItem.UseCase(itemsRepository);
      },
      inject: [ItemsRepository],
    },
  ],
  imports: [DataModule],
  exports: [ListItems.UseCase, AddItem.UseCase],
})
export class ItemsUcDi {}
