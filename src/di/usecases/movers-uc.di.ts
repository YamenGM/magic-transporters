import { AddMover, ListMovers } from '&/application/usecases/movers';
import { MoversRepository } from '&/domain/repositories';
import { DataModule } from '&/infrastructure/data/data.module';
import { Module } from '@nestjs/common';

@Module({
  providers: [
    {
      provide: ListMovers.UseCase,
      useFactory: (moversRepository: MoversRepository) => {
        return new ListMovers.UseCase(moversRepository);
      },
      inject: [MoversRepository],
    },
    {
      provide: AddMover.UseCase,
      useFactory: (moversRepository: MoversRepository) => {
        return new AddMover.UseCase(moversRepository);
      },
      inject: [MoversRepository],
    },
  ],
  imports: [DataModule],
  exports: [ListMovers.UseCase, AddMover.UseCase],
})
export class MoversUcDi {}
