import {
  AddMission,
  ListMissions,
  UpdateMission,
} from '&/application/usecases/missions';
import { GetMostCompletedMissions } from '&/application/usecases/missions/most-complated-missions.usecase';
import {
  ItemsRepository,
  MissionsRepository,
  MoversRepository,
} from '&/domain/repositories';
import { DataModule } from '&/infrastructure/data/data.module';
import { Module } from '@nestjs/common';

@Module({
  providers: [
    {
      provide: ListMissions.UseCase,
      useFactory: (missionsRepository: MissionsRepository) => {
        return new ListMissions.UseCase(missionsRepository);
      },
      inject: [MissionsRepository],
    },
    {
      provide: AddMission.UseCase,
      useFactory: (
        missionsRepository: MissionsRepository,
        moversRepository: MoversRepository,
        itemsRepository: ItemsRepository,
      ) => {
        return new AddMission.UseCase(
          missionsRepository,
          moversRepository,
          itemsRepository,
        );
      },
      inject: [MissionsRepository, MoversRepository, ItemsRepository],
    },
    {
      provide: UpdateMission.UseCase,
      useFactory: (
        missionsRepository: MissionsRepository,
        moversRepository: MoversRepository,
      ) => {
        return new UpdateMission.UseCase(missionsRepository, moversRepository);
      },
      inject: [MissionsRepository, MoversRepository],
    },
    {
      provide: GetMostCompletedMissions.UseCase,
      useFactory: (missionsRepository: MissionsRepository) => {
        return new GetMostCompletedMissions.UseCase(missionsRepository);
      },
      inject: [MissionsRepository],
    },
  ],
  imports: [DataModule],
  exports: [
    ListMissions.UseCase,
    AddMission.UseCase,
    UpdateMission.UseCase,
    GetMostCompletedMissions.UseCase,
  ],
})
export class MissionsUcDi {}
