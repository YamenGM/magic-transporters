export interface ConfigService {
  get<T>(key: string): T | undefined;
}

export const ConfigService = Symbol('ConfigService');
