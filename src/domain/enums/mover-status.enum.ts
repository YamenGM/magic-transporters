export enum MoverStatus {
  RESTING = 'RESTING',
  LOADING = 'LOADING',
  MISSION = 'MISSION',
  DONE = 'DONE',
}
