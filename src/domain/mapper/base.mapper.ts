export interface BaseMapper<Entity, Model> {
  toModel(entity: Entity): Model;
  toEntity(model: Model): Entity;
}
