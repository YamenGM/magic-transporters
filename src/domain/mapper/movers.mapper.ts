import { Mover } from '../entites';
import { BaseMapper } from './base.mapper';

export interface MoversMapper<MoverModel>
  extends BaseMapper<Mover, MoverModel> {}

export const MoversMapper = Symbol('MoversMapper');
