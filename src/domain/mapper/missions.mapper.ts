import { Mission } from '../entites';
import { BaseMapper } from './base.mapper';

export interface MissionsMapper<MissionModel>
  extends BaseMapper<Mission, MissionModel> {}

export const MissionsMapper = Symbol('MissionsMapper');
