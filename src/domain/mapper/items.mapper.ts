import { Item } from '../entites';
import { BaseMapper } from './base.mapper';

export interface ItemsMapper<ItemModel> extends BaseMapper<Item, ItemModel> {}

export const ItemsMapper = Symbol('ItemsMapper');
