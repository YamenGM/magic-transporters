import { Item } from '&/domain/entites';
import { IdOf } from '../types';
import { BaseRepository } from './base.repository';

export interface ItemsRepository extends BaseRepository {
  create(mover: Item): Promise<Item>;

  findMany(options?: ItemsRepository.FindAllOptions): Promise<Item[]>;

  findOne(filter: ItemsRepository.FindOneFilter): Promise<Item | null>;

  updateOne(item: Item): Promise<Item>;
}

export namespace ItemsRepository {
  export type FindOneFilter = {
    id?: IdOf<Item>;
  } & FindFilters;

  export type FindFilters = {};

  export type FindOptions = {
    relations?: {};
    withDeleted?: boolean;
  };

  export type FindAllOptions = {
    limit?: number;
    skip?: number;
  } & FindOptions;
}

export const ItemsRepository = Symbol('ItemsRepository');
