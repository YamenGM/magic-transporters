import { Mover } from '&/domain/entites';
import { IdOf } from '../types';
import { BaseRepository } from './base.repository';

export interface MoversRepository extends BaseRepository {
  create(mover: Mover): Promise<Mover>;

  findMany(options?: MoversRepository.FindAllOptions): Promise<Mover[]>;

  updateOne(mover: Mover): Promise<Mover>;

  findOne(filter: MoversRepository.FindOneFilter): Promise<Mover | null>;
}

export namespace MoversRepository {
  export type FindOneFilter = {
    id?: IdOf<Mover>;
  } & FindFilters;

  export type FindFilters = {};

  export type FindOptions = {
    relations?: {};
    withDeleted?: boolean;
  };

  export type FindAllOptions = {
    limit?: number;
    skip?: number;
  } & FindOptions;
}

export const MoversRepository = Symbol('MoversRepository');
