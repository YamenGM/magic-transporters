import { Mission } from '&/domain/entites';
import { ComplatedMissionType, IdOf } from '../types';
import { BaseRepository } from './base.repository';

export interface MissionsRepository extends BaseRepository {
  // Load a Magic Mover with items
  create(mission: Mission): Promise<Mission>;
  // Start a Mission & End a Mission
  updateOne(mission: Mission): Promise<Mission>;
  // simple list showing who completed the most missions
  getMostCompletedMissions(): Promise<ComplatedMissionType[]>;
  // list missions
  findMany(options?: MissionsRepository.FindAllOptions): Promise<Mission[]>;

  findOne(filter: MissionsRepository.FindOneFilter): Promise<Mission | null>;
}

export namespace MissionsRepository {
  export type FindOneFilter = {
    id?: IdOf<Mission>;
  } & FindFilters;

  export type FindFilters = {};

  export type FindOptions = {
    relations?: {};
    withDeleted?: boolean;
  };

  export type FindAllOptions = {
    limit?: number;
    skip?: number;
  } & FindOptions;
}

export const MissionsRepository = Symbol('MissionsRepository');
