import { IdOf } from '../types/id-of.type';
import { BaseEntity, IBaseEntity } from './base.entity';
import { Mission } from './mission.entity';

export class Item extends BaseEntity {
  // Props
  private _name: string;
  private _weight: number;

  // Foreign
  public missionId: IdOf<Mission> | null;

  // Relations
  private _mission?: Mission;

  constructor({
    id,
    name,
    weight,
    missionId,
    createdAt,
    deletedAt,
    updatedAt,
  }: IItem) {
    super({ id, createdAt, deletedAt, updatedAt });
    this._name = name;
    this._weight = weight;
    this.missionId = missionId;
  }

  public get name(): string {
    return this._name;
  }

  public get weight(): number {
    return this._weight;
  }
}

export interface IItem extends IBaseEntity {
  name: string;
  weight: number;
  missionId: IdOf<Mission> | null;
}
