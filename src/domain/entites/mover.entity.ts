import { MoverStatus } from 'src/domain/enums';
import { BaseEntity, IBaseEntity } from './base.entity';
import { Mission } from './mission.entity';

export class Mover extends BaseEntity {
  // Props
  private _weightLimit: number;
  private _energy: number;
  public questState: MoverStatus;

  // Relations
  private _missions?: Mission[];

  constructor({
    id,
    weightLimit,
    energy,
    questState,
    createdAt,
    deletedAt,
    updatedAt,
  }: IMover) {
    super({ id, createdAt, deletedAt, updatedAt });
    this._weightLimit = weightLimit;
    this._energy = energy;
    this.questState = questState;
  }

  public get weightLimit(): number {
    return this._weightLimit;
  }

  public get energy(): number {
    return this._energy;
  }

  public get missions(): Mission[] | undefined {
    return this._missions;
  }

  public update({ weightLimit, energy, questState }: Partial<IMover>): void {
    this._weightLimit = weightLimit || this._weightLimit;
    this._energy = energy || this._energy;
    this.questState = questState || this.questState;
    this.setUpdateDate(); /////////////////////ddddd
  }
}

export interface IMover extends IBaseEntity {
  weightLimit: number;
  energy: number;
  questState: MoverStatus;
}
