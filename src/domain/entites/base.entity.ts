export abstract class BaseEntity<IDType = number> {
  protected _id: IDType | undefined;

  protected _createdAt: Date;
  protected _updatedAt: Date;
  protected _deletedAt: Date | null;

  constructor({
    id,
    createdAt = new Date(),
    updatedAt = new Date(),
    deletedAt = null,
  }: IBaseEntity<IDType>) {
    this._id = id;
    this._createdAt = createdAt;
    this._updatedAt = updatedAt;
    this._deletedAt = deletedAt;
  }

  public get id(): IDType | undefined {
    return this._id;
  }

  public get createdAt(): Date {
    return this._createdAt;
  }

  public get updatedAt(): Date {
    return this._updatedAt;
  }

  public get deletedAt(): Date | null {
    return this._deletedAt;
  }

  public setUpdateDate(): void {
    this._updatedAt = new Date();
  }

  public setDeleteDate(): void {
    this._deletedAt = new Date();
  }

  public restoreFromDeletion(): void {
    this._deletedAt = null;
  }

  public isDeleted(): boolean {
    return this._deletedAt !== null;
  }
}

export interface IBaseEntity<IDType = number> {
  id: IDType | undefined;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date | null;
}
