import { IdOf } from '../types/id-of.type';
import { BaseEntity, IBaseEntity } from './base.entity';
import { Mover } from './mover.entity';
import { Item } from './item.entity';

export class Mission extends BaseEntity {
  // Props
  private _loadingAt: Date;
  public onMissionAt: Date | null;
  public doneAt: Date | null;

  // Foreign
  private _moverId: IdOf<Mover>;

  // Relations
  public mover?: Mover;
  public items?: Item[];

  constructor({
    id,
    loadingAt,
    onMissionAt,
    doneAt,
    moverId,
    createdAt,
    deletedAt,
    updatedAt,
  }: IMission) {
    super({ id, createdAt, deletedAt, updatedAt });
    this._loadingAt = loadingAt;
    this.onMissionAt = onMissionAt;
    this.doneAt = doneAt;
    this._moverId = moverId;
  }

  public get loadingAt(): Date {
    return this._loadingAt;
  }

  public get moverId(): IdOf<Mover> {
    return this._moverId;
  }
}

export interface IMission extends IBaseEntity {
  loadingAt: Date;
  onMissionAt: Date | null;
  doneAt: Date | null;
  moverId: IdOf<Mover>;
}
