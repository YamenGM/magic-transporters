import { Mission, Mover } from '../entites';
import { IdOf } from '../types/id-of.type';

export interface ComplatedMissionType {
  // Foreign
  moverId: IdOf<Mover>;
  missionsId?: IdOf<Mission>[];

  // Relations
  count: number;
}
