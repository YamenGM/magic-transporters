export type IdOf<T extends { id: unknown }> = Exclude<T['id'], undefined>;
