export class Page<T> {
  constructor(
    public readonly data: T[],
    public readonly page: number,
    public readonly perPage: number,
  ) {}
}
