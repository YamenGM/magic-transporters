declare namespace Express {
  interface Request {
    organizationId?: number;
    organizationSlug?: string;
    user?: unknown;
  }
}
