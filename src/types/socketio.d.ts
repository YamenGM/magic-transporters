import { Server } from 'socket.io';

declare module 'socket.io' {
  declare type NextFunction = Parameters<Parameters<Server['use']>[0]>[1];
}
