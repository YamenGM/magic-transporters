export * from './base.exception';
export * from './repository/entity-not-found.exception';
export * from './repository/repository.exception';
