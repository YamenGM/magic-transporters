interface BaseExceptionParams {
  message?: string;
  error?: Error;
}

export abstract class BaseException extends Error {
  public readonly error?: Error;

  constructor({ message, error }: BaseExceptionParams) {
    super(message);
    this.error = error;
  }
}
