import { BaseException } from '../base.exception';

interface RepositoryExceptionParams {
  message?: string;
  error?: Error;
}

export class RepositoryException extends BaseException {
  constructor({
    message = 'Repository exception',
    error,
  }: RepositoryExceptionParams = {}) {
    super({ message, error });
  }
}
