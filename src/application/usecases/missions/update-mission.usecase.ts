import { IdOf } from '&/domain/types';
import { BaseUseCase } from '../base.usecase';
import { MissionsRepository, MoversRepository } from '&/domain/repositories';
import { Mission } from '&/domain/entites';
import { MoverStatus } from '&/domain/enums';
import { EntityNotFoundException } from '&/application/exceptions';

export namespace UpdateMission {
  export class UseCase extends BaseUseCase<Input, Promise<Output>> {
    constructor(
      private readonly missionsRepository: MissionsRepository,
      private readonly moversRepository: MoversRepository,
    ) {
      super();
    }

    public async execute(input: Input): Promise<Output> {
      const mission = await this.missionsRepository.findOne({
        id: input.id,
      });

      if (!mission)
        throw new EntityNotFoundException({
          id: input.id,
          message: 'Mission not found',
        });

      if (
        (input.data.MissionStatus === 'start' &&
          (mission.doneAt || mission.onMissionAt || !mission.loadingAt)) ||
        (input.data.MissionStatus === 'end' &&
          (mission.doneAt || !mission.onMissionAt || !mission.loadingAt))
      ) {
        throw new EntityNotFoundException({
          id: input.id,
          message: `you want to ${input.data.MissionStatus} the mission but mission.doneAt: ${mission.doneAt}, mission.onMissionAt: ${mission.onMissionAt}, mission.loadingAt: ${mission.loadingAt}`,
        });
      }

      const mover = await this.moversRepository.findOne({
        id: mission.moverId,
      });

      if (input.data.MissionStatus === 'start') {
        mover.questState = MoverStatus.MISSION;
        await this.moversRepository.updateOne(mover);

        mission.onMissionAt = new Date();
        await this.missionsRepository.updateOne(mission);
      } else {
        mover.questState = MoverStatus.DONE;
        await this.moversRepository.updateOne(mover);

        mission.doneAt = new Date();
        await this.missionsRepository.updateOne(mission);
      }
      return (await this.missionsRepository.findMany()).find(
        (m) => m.id === mission.id,
      );
    }
  }

  export type Input = {
    id: IdOf<Mission>;
    data: {
      MissionStatus: 'start' | 'end';
    };
  };
  export type Output = Mission;
}
