import { ComplatedMissionType } from '&/domain/types';
import { BaseUseCase } from '../base.usecase';
import { MissionsRepository } from '&/domain/repositories';

export namespace GetMostCompletedMissions {
  export class UseCase extends BaseUseCase<Input, Promise<Output>> {
    constructor(private readonly missionsRepository: MissionsRepository) {
      super();
    }

    public async execute(input: Input): Promise<Output> {
      return await this.missionsRepository.getMostCompletedMissions();
    }
  }

  export type Input = {};
  export type Output = ComplatedMissionType[];
}
