import { BaseUseCase } from '../base.usecase';
import {
  ItemsRepository,
  MissionsRepository,
  MoversRepository,
} from '&/domain/repositories';
import { Item, Mission } from '&/domain/entites';
import { MoverStatus } from '&/domain/enums';
import { EntityNotFoundException } from '&/application/exceptions';

export namespace AddMission {
  export class UseCase extends BaseUseCase<Input, Promise<Output>> {
    constructor(
      private readonly missionsRepository: MissionsRepository,
      private readonly moversRepository: MoversRepository,
      private readonly itemsRepository: ItemsRepository,
    ) {
      super();
    }

    public async execute(input: Input): Promise<Output> {
      const mover = await this.moversRepository.findOne({
        id: input.moverId,
      });

      if (
        !mover ||
        (mover.questState !== MoverStatus.DONE &&
          mover.questState !== MoverStatus.RESTING)
      )
        throw new EntityNotFoundException({
          id: input.moverId,
          message: 'Mover not found OR Mover is not DONE or RESTING',
        });

      const items: Item[] = [];
      for (let i = 0; i < input.itemsId.length; i++) {
        const item = await this.itemsRepository.findOne({
          id: input.itemsId[i],
        });

        if (!item || item.missionId)
          throw new EntityNotFoundException({
            id: input.itemsId[i],
            message: 'Item not found Or linked with mission',
          });

        items.push(item);
      }

      const magicPowerNeed = items.reduce(
        (partialSum, a) => partialSum + a.weight,
        0,
      );

      if (magicPowerNeed > mover.weightLimit)
        throw new EntityNotFoundException({
          id: input.moverId,
          message: `Magic Movers too much to carry, magicPowerNeed: ${magicPowerNeed}, mover.weightLimit: ${mover.weightLimit}`,
        });

      const mission = new Mission({
        id: Math.floor(Math.random() * 1000000),
        loadingAt: new Date(),
        doneAt: null,
        moverId: input.moverId,
        onMissionAt: null,
        createdAt: new Date(),
        updatedAt: new Date(),
        deletedAt: null,
      });
      mission.mover = mover;
      mission.items = items;

      await Promise.all(
        mission.items.map(async (item) => {
          const newItem = new Item(item);
          newItem.missionId = mission.id;
          return await this.itemsRepository.updateOne(newItem);
        }),
      );

      mission.mover.questState = MoverStatus.LOADING;
      await this.moversRepository.updateOne(mission.mover);

      return await this.missionsRepository.create(mission);
    }
  }

  export type Input = {
    moverId: number;
    itemsId: number[];
  };
  export type Output = Mission;
}
