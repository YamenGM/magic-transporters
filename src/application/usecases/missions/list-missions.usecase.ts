import { MissionsRepository } from '&/domain/repositories';
import { Page } from '&/domain/types';
import { paramsToPage } from '&/application/utils/pagination';
import { BaseUseCase } from '../base.usecase';
import { Mission } from '&/domain/entites';

export namespace ListMissions {
  export class UseCase extends BaseUseCase<Input, Promise<Output>> {
    constructor(private readonly missionsRepository: MissionsRepository) {
      super();
    }

    public async execute(input: Input): Promise<Output> {
      const { page, perPage } = input;

      const { limit, skip } = paramsToPage(page, perPage);

      const missions = await this.missionsRepository.findMany({ limit, skip });

      return new Page(missions, page, perPage);
    }
  }

  export type Input = {
    page: number;
    perPage: number;
  };

  export type Output = Page<Mission>;
}
