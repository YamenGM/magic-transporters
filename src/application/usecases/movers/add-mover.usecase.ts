import { BaseUseCase } from '../base.usecase';
import { MoversRepository } from '&/domain/repositories';
import { Mover } from '&/domain/entites';
import { MoverStatus } from '&/domain/enums';

export namespace AddMover {
  export class UseCase extends BaseUseCase<Input, Promise<Output>> {
    constructor(private readonly moversRepository: MoversRepository) {
      super();
    }

    public async execute(input: Input): Promise<Output> {
      const mover = new Mover({
        ...input,
        id: Math.floor(Math.random() * 1000000),
        questState: MoverStatus.RESTING,
        createdAt: new Date(),
        updatedAt: new Date(),
        deletedAt: null,
      });

      return await this.moversRepository.create(mover);
    }
  }

  export type Input = {
    weightLimit: number;
    energy: number;
  };
  export type Output = Mover;
}
