import { MoversRepository } from '&/domain/repositories';
import { Page } from '&/domain/types';
import { paramsToPage } from '&/application/utils/pagination';
import { BaseUseCase } from '../base.usecase';
import { Mover } from '&/domain/entites';

export namespace ListMovers {
  export class UseCase extends BaseUseCase<Input, Promise<Output>> {
    constructor(private readonly moversRepository: MoversRepository) {
      super();
    }

    public async execute(input: Input): Promise<Output> {
      const { page, perPage } = input;

      const { limit, skip } = paramsToPage(page, perPage);

      const movers = await this.moversRepository.findMany({ limit, skip });

      return new Page(movers, page, perPage);
    }
  }

  export type Input = {
    page: number;
    perPage: number;
  };

  export type Output = Page<Mover>;
}
