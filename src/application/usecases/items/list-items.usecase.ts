import { ItemsRepository } from '&/domain/repositories';
import { Page } from '&/domain/types';
import { paramsToPage } from '&/application/utils/pagination';
import { BaseUseCase } from '../base.usecase';
import { Item } from '&/domain/entites';

export namespace ListItems {
  export class UseCase extends BaseUseCase<Input, Promise<Output>> {
    constructor(private readonly itemsRepository: ItemsRepository) {
      super();
    }

    public async execute(input: Input): Promise<Output> {
      const { page, perPage } = input;

      const { limit, skip } = paramsToPage(page, perPage);

      const items = await this.itemsRepository.findMany({ limit, skip });

      return new Page(items, page, perPage);
    }
  }

  export type Input = {
    page: number;
    perPage: number;
  };

  export type Output = Page<Item>;
}
