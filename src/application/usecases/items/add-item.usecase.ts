import { BaseUseCase } from '../base.usecase';
import { ItemsRepository } from '&/domain/repositories';
import { Item } from '&/domain/entites';

export namespace AddItem {
  export class UseCase extends BaseUseCase<Input, Promise<Output>> {
    constructor(private readonly itemsRepository: ItemsRepository) {
      super();
    }

    public async execute(input: Input): Promise<Output> {
      const item = new Item({
        ...input,
        id: Math.floor(Math.random() * 1000000),
        missionId: null,
        createdAt: new Date(),
        updatedAt: new Date(),
        deletedAt: null,
      });

      return await this.itemsRepository.create(item);
    }
  }

  export type Input = {
    name: string;
    weight: number;
  };
  export type Output = Item;
}
